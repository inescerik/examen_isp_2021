package Examen.Subiectu1;

import java.util.ArrayList;
import java.util.List;

public class Sub1 {

    public static void main(String[] args) {
    }
}

class H{
    float n;
    void f(int g){
    };
}

class I extends H{
    long t;
    @Override
    void f(int g)
    {
    }
}

class J{
    void i(I ii)
    {

    }
}

class K{
    private ArrayList<L> ls= new ArrayList<L>(
            List.of(new L())
    );
    private ArrayList<M> ms;
}

class L{
    void metA(){

    }
}

class M{
    void metB(){

    }
}