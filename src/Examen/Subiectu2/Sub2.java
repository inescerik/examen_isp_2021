package Examen.Subiectu2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Scanner;

public class Sub2 {

    public static void main(String[] args) {
        JFrame frame = new JFrame();

        JTextField jTextField = new JTextField();

        JButton button = new JButton("Button");

        JPanel panel = new JPanel();

        panel.setLayout(new GridLayout(10, 1));

        panel.add(jTextField);

        panel.add(button);

        frame.setContentPane(panel);

        frame.setSize(500, 500);

        frame.setVisible(true);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String texto = jTextField.getText();
                System.out.println(texto);
                ArrayList<String> lines = new ArrayList<>();
                try {
                    File fisier = new File(texto);
                    Scanner scan = new Scanner(fisier);
                    while (scan.hasNextLine()) {

                        String data = scan.nextLine();
                        lines.add(data);
                        //System.out.println(data);
                    }
                    for(int i=lines.size()-1;i>=0;--i)
                    {
                        StringBuilder aux= new StringBuilder();
                        aux.append(lines.get(i));
                        System.out.println(aux.reverse());
                    }
                    scan.close();

                } catch (FileNotFoundException ec) {
                    System.out.println("An error occurred.");
                    ec.printStackTrace();
                }

            }
        });
    }
}